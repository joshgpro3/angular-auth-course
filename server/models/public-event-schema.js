//THIS IS A SCHEMA OBJECT
const mongoose = require('mongoose');

const publicEventSchema = new mongoose.Schema({
    name: String,
    description: String,
    type: String, //public or special
    date: String    
});

module.exports = mongoose.model('public-event-schema', publicEventSchema, 'events-public');
//in the above we have model(the file name, the schema created, the relevant collection in databse)
//connect in the api.js

//Sample data
// {
//     "name": "Art Exhibition",
//     "description": "Digital visual art in galleries",
//     "type":"public",
//     "date": "2020-09-23"
// }