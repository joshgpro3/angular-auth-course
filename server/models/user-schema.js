//THIS IS A SCHEMA OBJECT
const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    email: String,
    password: String
});

module.exports = mongoose.model('user-schema', userSchema, 'users');
//in the above we have model(the file name, the schema created, the relevant collection in databse)
//connect in the api.js