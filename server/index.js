//Install expressjs and body-parser: npm install express body-parser --save
//Optionally install nodemon for dev. NB: SERVER FILENAME MUST BE "index.js"!
//1 - import express and body-parser
//2 - create port variable
//3 - create server app object from express
//4 - Use body-parser middle ware
//5 - Get route function or use router imported from the actual route containing code
//6 - Listen on the port
//Start the server: "npm start" or "npx nodemon"
//Navigate to the root displayed on the console.

const express = require('express');
const bodyParser = require('body-parser');
const router = require('./routes/api.js');

const PORT = process.env.PORT || 3000;
const app = express();

app.use(bodyParser.json());

app.use(router);
//The above passes the requests to the router file.
//the app.get function is routed from another file /routes/api.js containing the exported route.

app.listen(PORT, () => console.log(`Server is running on localhost:${PORT}`));

