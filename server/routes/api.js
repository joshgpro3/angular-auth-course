const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const PublicEventSchema = require('../models/public-event-schema');
const PrivateEventSchema = require('../models/private-event-schema');
const UserSchema =  require('../models/user-schema');

const db = 'mongodb+srv://userjosh:passwordjosh@cluster0.ukiwy.mongodb.net/eventsdb?retryWrites=true&w=majority';

mongoose.connect(db, { useNewUrlParser: true, useUnifiedTopology: true }, err => {
    if (err){
        console.log(`Error connecting to database: ${err}`);
    } else {
        console.log(`Connected to mongo db`); 
    }
});

//Endpoint for root
router.get('/',(req, res)=>{
    res.send(`
    <div id="container" style="height:100%; width:100%; background-color:teal;">
        <div id="header" style="padding:20px;">
            <h1 style="color:white;">Hello from Server in Routes</h1>
            <h3> Joshua Arosanyin</h3>
            <hr/>
        </div>
    </div>
    `)
});

//Endpoint for new user account registration
router.post('/api/register', (req, res)=>{
    //Extract user information from request body.
    //Use the schema object to cast (or structurize) the request body
    let user = new UserSchema(req.body);
    //Save the values to the database using mongoose SAVE method
    user.save((error, registeredUser)=>{
        if(error){
            console.log(`Error: could not create user: ${error}`); 
        } else {
            res.status(200).send(registeredUser);
        }
    });

});

//Endpoint for user login (test various scenarios with postman)
router.post('/api/login', (req, res)=>{    
    //Check if username in the db matches username in request using mongoose findOne method
    //NB: THE findOne METHOD IS ON THE MODEL SCHEMA!!!!!!!!!! [strange!!!] previously created
    //Since the model schema is being used, no need to cast or structurize with the schema
    //If it matches, then check if passwords match, and give responses.
    //
    //findOne accepts a condition and returns the data if found: See syntax below:
    //findOne({maching condition}, (error if not found, user if found))
    UserSchema.findOne({email: req.body.email}, (error, user)=>{
        if (!user){
            res.status(401).send('Invalid email');
        } else if (user.password !== req.body.password){
            res.status(401).send('Invalid password');
        } else {
            res.status(200).send(user);
        }
    });
});

//PUBLIC EVENT Create 
router.post('/api/create-public-event', (req, res)=>{
    let event = new PublicEventSchema(req.body);
    //Save the values to the database using mongoose SAVE method
    event.save((error, createdEvent)=>{
        if(error){
            console.log(`Error: could not create event: ${error}`); 
        } else {
            res.status(200).send(createdEvent);
        }
    });
}); 


//Public Events get all 
router.get('/api/public-events', (req, res)=>{ 
    PublicEventSchema.find({}, (error, events)=>{
        res.send(events);
    });     
    
});

//PRIVATE EVENT Create 
router.post('/api/create-private-event', (req, res)=>{
    let event = new PrivateEventSchema(req.body);
    //Save the values to the database using mongoose SAVE method
    event.save((error, createdEvent)=>{
        if(error){
            console.log(`Error: could not create event: ${error}`); 
        } else {
            res.status(200).send(createdEvent);
        }
    });
}); 

//Private Events get all 
router.get('/api/private-events', (req, res)=>{ 
    PrivateEventSchema.find({}, (error, events)=>{
        res.send(events);
    });
});


module.exports = router;