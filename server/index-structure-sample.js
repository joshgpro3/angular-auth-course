//Install expressjs and body-parser: npm install express body-parser --save
//Optionally install nodemon for dev. NB: SERVER FILENAME MUST BE "index.js"!
//1 - import express and body-parser
//2 - create port variable
//3 - create server app object from express
//4 - Use body-parser middle ware
//5 - Get route function
//6 - Listen on the port
//Start the server: "npm start" or "npx nodemon"
//Navigate to the root displayed on the console.

const express = require('express');
const bodyParser = require('body-parser');

const PORT = process.env.PORT || 3000;
const app = express();

app.use(bodyParser.json());

app.get('/',(req, res) => res.send('<h1>Hello from server</h1>'));

//OR USE BACKTICKS TO ENTER FULL TEMPLATE IN RESPONSE
/* 
app.get('/',(req, res) => {
    res.send(`
    <div id="container" style="height:100%; width:100%; background-color:teal;">
        <div id="header" style="padding:20px;">
            <h1 style="color:white;">Hello from Server in Routes</h1>
            <h3> Joshua Arosanyin</h3>
            <hr/>
        </div>
    </div>
    `)
});
*/

app.listen(PORT, () => console.log(`Server is running on localhost:${PORT}`));

